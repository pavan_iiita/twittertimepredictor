package Run;

import twitter4j.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.*;

/**
 * Created by pavan on 10/1/16.
 */
public class Main extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        long id;
        Twitter twitter = TwitterFactory.getSingleton();
        try {
            int flag = Integer.parseInt(request.getParameter("flag"));
            if(flag==1) id = (twitter.showUser(request.getParameter("user"))).getId();
            else id = Long.parseLong(request.getParameter("user"));
        }
        catch (Exception e) {
            e.printStackTrace();
            id=101809484L;
        }
        //System.out.println(id);
        out.write(Process.run(id));
    }

    public static void main(String[] args) throws TwitterException {
        System.out.println(Process.run(2592821390L));
    }

}
