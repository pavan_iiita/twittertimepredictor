'use strict';
var app = angular.module('TwitterApp',[]);
app.controller("TwitterCtrl",["$rootScope", "$scope","$http", function($rootScope, $scope, $http){
    $scope.detail={
        userId:"",
        screenName:""
    };
    var days = new Array();
        days[0] = "";
        days[1] = "Sunday";
        days[2] = "Monday";
        days[3] = "Tuesday";
        days[4] = "Wednesday";
        days[5] = "Thursday";
        days[6] = "Friday";
        days[7] = "Saturday";

    var timeSlot = new Array();
        timeSlot[0] = "12 AM - 1 AM";
        timeSlot[1] = "1 AM - 2 AM";
        timeSlot[2] = "2 AM - 3 AM";
        timeSlot[3] = "3 AM - 4 AM";
        timeSlot[4] = "4 AM - 5 AM";
        timeSlot[5] = "5 AM - 6 AM";
        timeSlot[6] = "6 AM - 7 AM";
        timeSlot[7] = "7 AM - 8 AM";
        timeSlot[8] = "8 AM - 9 AM";
        timeSlot[9] = "9 AM - 10 AM";
        timeSlot[10] = "10 AM - 11 AM";
        timeSlot[11] = "11 AM - 12 PM";
        timeSlot[12] = "12 PM - 1 PM";
        timeSlot[13] = "1 PM - 2 PM";
        timeSlot[14] = "2 PM - 3 PM";
        timeSlot[15] = "3 PM - 4 PM";
        timeSlot[16] = "4 PM - 5 PM";
        timeSlot[17] = "5 PM - 6 PM";
        timeSlot[18] = "6 PM - 7 PM";
        timeSlot[19] = "7 PM - 8 PM";
        timeSlot[20] = "8 PM - 9 PM";
        timeSlot[21] = "9 PM - 10 PM";
        timeSlot[22] = "10 PM - 11 PM";
        timeSlot[23] = "11 PM - 12 PM";

    $scope.sendAjaxForPost = function ( pUrl, successCallback , failureCallback) {
        $http({
            method: "POST", dataType: "json", url: pUrl
        }).success(function (data) {
            if (data.success || failureCallback == undefined) {
                successCallback(data);
            }
            else{
                failureCallback(data);
            }
        }).error(function(data, status, headers, config) {

            failureCallback(data);
        });
    };


    $scope.succesCallBack=function(recievedObj){
        console.log(recievedObj);

    }
    $scope.failureCallBack=function(recievedObj){
        $scope.resArr=[];
        console.log(recievedObj);
        var resScore=0;
        var resIndex=0;

        for(var i=0;i<recievedObj.length;i++){
            if(recievedObj[i].score>resScore){
                resScore=recievedObj[i].score;
                resIndex=i;
            }
            var resObj={
                day:"",
                time:"",
                flag:""
            };
            console.log(days[recievedObj[i].day]);
            console.log(timeSlot[recievedObj[i].timeslot]);
            resObj.day=days[recievedObj[i].day];
            resObj.time     =timeSlot[recievedObj[i].timeslot];
            resObj.flag=0;
            console.log(resObj);
            console.log($scope.resArr);
            $scope.resArr.push(resObj);
        }
        $scope.resArr[resIndex].flag=1;
        console.log($scope.resArr);
    };
    $scope.setColor=function(obj){
        if(obj.flag==1){
            return  {'background-color':'#FDBCB4'} ;
        }
        else{
            return  {'background-color':'#FFDEAD'} ;
        }
    };

    $scope.fetch=function(data){
        console.log(data);
        var obj={
            flag:"",
            detail:""
        };
        if(data.userId != ""){
            obj.flag=0;
            obj.detail = data.userId;
        }else{
            obj.flag=1;
            obj.detail = data.screenName;
        }
        console.log(obj);
        var url="http://twitter-test61292.rhcloud.com/getbesttime?flag="+obj.flag+"&user="+obj.detail;
        $scope.sendAjaxForPost(url,$scope.succesCallBack,$scope.failureCallBack)

    };

}]);