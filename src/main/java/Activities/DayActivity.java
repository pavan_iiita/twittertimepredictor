package Activities;

import twitter4j.TwitterException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by pavan on 11/1/16.
 */
public class DayActivity {
    public static long dayDiff(Date date) {
        Date today = new Date();
        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");

        String dateString = myFormat.format(date);
        String todayString = myFormat.format(today);
        long days=0;
        try {
            Date date1 = myFormat.parse(dateString);
            Date date2 = myFormat.parse(todayString);
            long diff = date2.getTime() - date1.getTime();
            days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return days;
    }
    public static ArrayList<Date>[] getDaywiseData(ArrayList<Date> activityTimestampList) {
        ArrayList<Date> dayData[] = new ArrayList[30];
        for(int i=0;i<30;i++) dayData[i]=new ArrayList<Date>();
        for(Date date : activityTimestampList) {
            //System.out.println(date+"))");
            long days = dayDiff(date);
            if(days > 28) continue;
            //System.out.println(date+" "+days);
            dayData[(int)days].add(date);
        }
        return dayData;
    }
}
