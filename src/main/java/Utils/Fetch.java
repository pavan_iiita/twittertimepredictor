package Utils;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by pavan on 10/1/16.et
 */

public class Fetch {
    public static ArrayList<Date> getTweetData(Long id){
        //System.out.println(id);
        ArrayList<Date> tweetResult = new ArrayList<Date>();
        try {
            Twitter twitter = TwitterFactory.getSingleton();
            Paging paging = new Paging(1,200);
            ResponseList<Status> responseList = twitter.getUserTimeline(id,paging);
            for(Status s : responseList)
                tweetResult.add(s.getCreatedAt());
        }
        catch (TwitterException e) {
            if(!(e.getStatusCode()==HttpResponseCode.UNAUTHORIZED || e.getStatusCode() == HttpResponseCode.NOT_FOUND))
                e.printStackTrace();
        }
        return tweetResult;
    }
    public static ArrayList<Date> getLikeData(Long id){
        ArrayList<Date> likeResult = new ArrayList<Date>();
        /*try {
            Twitter twitter = TwitterFactory.getSingleton();
            Paging paging = new Paging(1,200);
            ResponseList<Status> responseList = twitter.getFavorites(id, paging);
            for(Status s : responseList)
                likeResult.add(s.getCreatedAt());
        }
        catch (Exception e) {
            e.printStackTrace();
        }*/
        return likeResult;
    }
    public static ArrayList<Date> getData(Long id) {
        //System.out.println("getting data for : "+id);
        ArrayList<Date> resultList = new ArrayList<Date>();
        ArrayList<Date> likeResult = getLikeData(id);
        ArrayList<Date> tweetResult = getTweetData(id);
        //System.out.println(likeResult.size()+" "+tweetResult.size());
        for(Date date : likeResult) resultList.add(date);
        for(Date date : tweetResult) resultList.add(date);
        return resultList;
    }

    public static void main(String[] args) throws TwitterException {
        /*Twitter twitter = TwitterFactory.getSingleton();
        System.out.println(twitter.showUser(2821275845L).getScreenName());*/
        ArrayList<Date> a = getTweetData(101809484L);
        for(Date d : a) {
            System.out.println(d);
        }
    }
}
