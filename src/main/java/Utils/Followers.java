package Utils;

import twitter4j.IDs;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;

import java.util.ArrayList;

/**
 * Created by pavan on 10/1/16.
 */
public class Followers {
    public static ArrayList<Long> get(long id) {
        Twitter twitter = TwitterFactory.getSingleton();
        long cursor =-1L;
        ArrayList<Long> idList = new ArrayList<Long>();
        IDs ids;
        try {
            ids = twitter.getFollowersIDs(id,cursor,5000);
            for(Long userId : ids.getIDs()){
                idList.add(userId);
            }
            /*do {
            } while((cursor = ids.getNextCursor())!=0 );*/
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return idList;
    }
}
