package Run;

import Activities.Activity;
import Beans.resultSet;
import Beans.resultSetWeek;
import Utils.Followers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by pavan on 10/1/16.
 */
public class Process {
    public static final int prb=1;
    public static String run(long id) {
        Date dt = new Date();
        System.out.println(dt);
        ArrayList<Long> followersIdList = Followers.get(id);
        System.out.println("followers size = "+followersIdList.size());
        int p=0;
        int day[][] = new int [8][24];
        for(Long followerId : followersIdList){
            if(p>500) break;
            p++;
            try {
                ArrayList<int[]> weeklyDistribution = Activity.getTimeslot(followerId);
                for(int j=0;j<24;j++) {
                    for(int k=1;k<=7;k++) {
                        day[k][j]+=(weeklyDistribution.get(k-1)[j]>=prb)?1:0;
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        resultSetWeek rsw = new resultSetWeek();
        rsw.resultSetList = new ArrayList<resultSet>();

        for(int j=1;j<=7;j++) {
            int ans=-1;
            int pos=-1;
            for(int i=0;i<24;i++) {
                if(day[j][i]>ans) {
                    ans=day[j][i];
                    pos=i;
                }
            }
            resultSet rs = new resultSet(j,pos,ans);
            rsw.resultSetList.add(rs);
        }
        Gson gson = new GsonBuilder().create();
        String result = gson.toJson(rsw);
        System.out.println(result);
        return result;
    }
}