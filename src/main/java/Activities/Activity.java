package Activities;

import Utils.Fetch;
import twitter4j.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by pavan on 10/1/16.
 */
public class Activity {
    public static int getDayId() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }
    public static ArrayList<int[]> getTimeslot(Long id) throws TwitterException {
        //System.out.println(id);
        ArrayList<Date> activityTimestampList = Fetch.getData(id);
        //System.out.println(activityTimestampList.size());
        //for(Date date : activityTimestampList) System.out.print(date+" ");
        //System.out.println();
        ArrayList<Date> dayData[] = DayActivity.getDaywiseData(activityTimestampList);
        /*for(int i=1;i<=28;i++) {
            for(Date date : dayData[i]) {
                System.out.print(date + " ");
            }
            System.out.println();
        }*/
        int globalDayList[][]=new int[8][24];
        int dayId = getDayId();
        for(int i=1;i<=28;i++) {
            int intervalList[] = HourActivity.getHourwiseTimeslot(dayData[i]);
            /*if(i==1) System.out.println("**************************");
            for(int j=0;j<24;j++) System.out.print(intervalList[j]+" ");
            System.out.println();*/
            dayId--;
            if(dayId==0) dayId=7;
            //System.out.println("dayIdNew = "+dayId+" i = "+i);
            for(int j=0;j<24;j++) globalDayList[dayId][j]+=intervalList[j];
        }
        ArrayList<int []> weeklyDistribution = new ArrayList<int[]>();
        for(int i=1;i<=7;i++) {
            weeklyDistribution.add(globalDayList[i]);
            /*for(int j=0;j<24;j++) System.out.print(globalDayList[i][j]+" ");
            System.out.println();*/
        }
        return weeklyDistribution;
    }

    public static void main(String[] args) throws TwitterException {
        ArrayList<int []> a = getTimeslot(18839785L);
        for(int[] data : a){
            for(int d: data){
                System.out.print(d);
            }
            System.out.println("\n");
        }
        System.out.println("\n");
    }
}
