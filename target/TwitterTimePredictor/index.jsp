<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <title>Twitter App</title>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js" type="text/javascript"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-route.min.js" type="text/javascript"></script>
    <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.js" type="text/javascript"></script>

</head>
<body ng-app="TwitterApp">
<div class="container" ng-controller="TwitterCtrl"><br><br>
    <div class="row">
        <div class="col-md-offset-4 col-xs-offset-4">
            <div class="panel panel-primary col-md-5 col-xs-5"><br>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 ">
                        <b>UserId:</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1">
                        <input type="text"
                               ng-model="detail.userId"
                               class="form-control"
                               placeholder="Enter UserId"
                               required>
                    </div>
                </div><br>

                <div class = "row">
                    <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1">
                        <b>Screen Name</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1">
                        <input type="text"
                               ng-model="detail.screenName"
                               placeholder="Enter Screen Name"
                               class="form-control"
                               required
                                >
                    </div>
                </div><br>

                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-xs-4 col-xs-offset-4">
                        <button class="btn btn-primary" type="button"  ng-click = "fetch(detail)">Fetch</button>
                    </div>
                </div><br>
            </div>
        </div>
    </div><br><hr>
    <h3>Result</h3><hr>
    <div ng-repeat="obj in resArr">
        <div class="row" ng-style="setColor(obj)">
            <div class="col-md-3 col-md-offset-2 col-xs-3 col-xs-offset-2">{{obj.day}}</div>
            <div class="col-md-3 col-md-offset-2 col-xs-3 col-xs-offset-2">{{obj.time}}</div>
        </div><br>
    </div>
</div>

<script src="twitter.js" type="text/javascript"></script>
</body>
</html>