package Activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by pavan on 11/1/16.
 */
public class HourActivity {
    public static final int buffer = 10;
    public static long getMinuteId(Date date) {
        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
        String time = localDateFormat.format(date);
        long hour = (time.charAt(0)-48)*10+(time.charAt(1)-48);
        long minute = (time.charAt(3)-48)*10+(time.charAt(4)-48);
        minute += hour*60;
        return minute;
    }

    public static ArrayList<Long> getHourwiseData(ArrayList<Date> dayData) {
        ArrayList<Long> timestampList = new ArrayList<Long>();
        for(Date date : dayData) timestampList.add(getMinuteId(date));
        return timestampList;
    }

    public static int[] getHourwiseTimeslot(ArrayList<Date> dayData) {
        int arr[] = new int[24];
        ArrayList<Long> timestampList = getHourwiseData(dayData);
        for(Long timestamp : timestampList) {
            arr[(int)(timestamp/60)]=1;
        }
        return arr;
    }
}
